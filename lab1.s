# Justin Tumidanski
# Andrew Markiewicz
#
# CS3421 Lab 1
# Spring 2010
#
# This program reads two integers, then outputs all integers between them that
# consist of all the same digits.
.text
	lw		$s0,y			#stored value 1
	lw		$k0,z			#digit count
	lw		$k1,z			#digit count

	li		$v0,5			#read input from user
	syscall					#syscall
	add		$t0,$v0,$zero	#store input in $t0

	li		$v0,5			#read input again from user
	syscall					#syscall
	add		$t1,$v0,$zero	#store input in $t1

#
#Figure out the number of digits for the first input
#
	add		$t2,$t0,$zero	#store reference to first input
	lw		$t8,x			#store value of 10
loop1:
	div		$t2,$t2,$t8		#divide first input by 10
	add		$k0,$k0,$s0		#increment value by 1

	blt		$t2,$s0,exit1	#if the input is greater than 1 repeat
	b		loop1
exit1:

#
#Figure out the number of digits for the second input
#
	add		$t3,$t1,$zero	#store reference to first input
loop2:
	div		$t3,$t3,$t8		#divide first input by 10
	add		$k1,$k1,$s0		#increment value by 1

	blt		$t3,$s0,exit2	#if the input is greater than 1 repeat
	b		loop2
exit2:
	
#	sub		$t5,$k0,$s0	#move offset into known place
#   add   	$t5,$t5,$t5
#   add  	$t5,$t5,$t5
#	lw    	$t6,table($t5)





	add		$s2,$k0,$zero			#Initializing temp variable i as first number of digits
forloop1:							#beginning of first for loop
	bgt		$s2,$k1,endforloop1		#if i>number of digits in second input exit
	
	lw		$s7,w					#establishing the #9
	add		$s3,$s0,$zero			#Initializing temp variable j as 1
	forloop2:						#beginning of second for loop
	bgt		$s3,$s7,endforloop2		#if j>9 end the for loop

	sub		$s4,$s2,$s0				#move offset into known place
    add   	$s4,$s4,$s4				#fix offset
    add  	$s4,$s4,$s4				#fix offset
	lw    	$s5,table($s4)			#store the array value at the offset
	mul		$s5,$s5,$s3				#multiply the array value by j
	
	if:
	ble		$s5,$t0,skip			#if the array value multiplied by j is less than input1 don't do anything 
	bge		$s5,$t1,skip			#if	the array value multiplied by j is greater than input2 don't do anything

	add   $a0,$s5,$zero				#move value to be printed out into $a0
	li    $v0,1						#print out integer
	syscall							#syscall


    la    $a0,newline				#start a new line
    li    $v0,4
    syscall

	skip:
	add		$s3,$s3,$s0				#increment variable j
	b		forloop2				#re-run forloop2
	endforloop2:

	add		$s2,$s2,$s0				#increment variable i
	b		forloop1				#re-run forloop1
endforloop1:

.data
	w:	.word	9
	x:	.word	10
	y:	.word	1
	z:	.word	0

table:   
        .word   1
        .word   11
        .word   111
        .word   1111
        .word   11111
        .word   111111
        .word   1111111

newline:
		.asciiz "\n"
