# Justin Tumidanski
# Andrew Markiewicz
#
# CS3421 Lab 2
# Spring 2010
#
# This program reads a single assemby language instruction and outputs the
# encoded version of that instruction.

.text

#Read in a string from the user
lw	$a1,x					#allocate the maximum size of the string
la	$a0,msg					#load the place in memory the string will be stored into $t0
li	$v0,8					#read in a string from the user
syscall						#syscall

#Determine which operation the user wants encoded
la	$t0,msg					#load the address of the stored string
lb	$t1,0($t0)				#loads the first character in the string into $t1
lw	$a3,offset				#loads the offset
lw  $t2,array1($a3)			#loads the first content of the table
beq	$t1,$t2,charA			#if the first character is an 'a' goto the a area to further identify

lw	$a2,y					#loads the number 4
add $a3,$a3,$a2				#increase offset by 4
lw  $t2,array1($a3)			#loads the second content of the table
beq	$t1,$t2,oBranchEqual	#if the first character is an 'b' goto branch equal

add $a3,$a3,$a2				#increase offset by 4
lw  $t2,array1($a3)			#loads the third content of the table
beq	$t1,$t2,oJump			#if the first character is an 'j' goto jump

add $a3,$a3,$a2				#increase offset by 4
lw  $t2,array1($a3)			#loads the third content of the table
beq	$t1,$t2,oLoadWord		#if the first character is an ''l goto load word

add $a3,$a3,$a2				#increase offset by 4
lw  $t2,array1($a3)			#loads the third content of the table
beq	$t1,$t2,oOr				#if the first character is an 'o' goto or

add $a3,$a3,$a2				#increase offset by 4
lw  $t2,array1($a3)			#loads the third content of the table
beq	$t1,$t2,charS			#if the first character is an 's' goto the s area to further identify
b	end						#if the user inputs an unrecognizable operation don't do anything

charA:						#do stuff if the first character is an 'a'
la	$t0,msg					#load the address of the stored string
lb	$t1,1($t0)				#loads the second character in the string into $t1
lw	$a3,offset				#loads the offset
lw  $t2,array2($a3)			#loads the first content of the table
beq	$t1,$t2,oAdd			#if the second character is a d goto 'add'

lw	$a2,y					#loads the number 4
add $a3,$a3,$a2				#increase offset by 4
lw  $t2,array2($a3)			#loads the second content of the table
beq	$t1,$t2,oAnd			#if the second character is a n goto 'and'
b end						#If they are neither an a or a d then its a invalid input don't do anything


charS:						#do stuff if the first character is an 's'
la	$t0,msg					#load the address of the stored string
lb	$t1,1($t0)				#loads the second character in the string into $t1
lw	$a3,offset				#loads the offset
lw  $t2,array3($a3)			#loads the first content of the table
beq	$t1,$t2,oSubtract		#if the second character is a 'u' goto 'subtract'

lw	$a2,y					#loads the number 4
add $a3,$a3,$a2				#increase offset by 4
lw  $t2,array3($a3)			#loads the second content of the table
beq	$t1,$t2,oSetLess		#if the second character is a 'l' goto 'setless'

add $a3,$a3,$a2				#increase offset by 4
lw  $t2,array3($a3)			#loads the third content of the table
beq	$t1,$t2,oStoreWord		#if the first character is a 'w' goto 'store word'
b end						#If they are neither a u or a l or a w then its a invalid input don't do anything








oAdd:
li  $a3,0
lw  $s7,opcodes($a3)		#loads adds opcode into a known place in memory

la	$t0,msg					#load the address of the stored string

####################Encode Register 1###########################
lw	$t2,z					#loads the '$' character

findLoop1:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop1	#Looks for the '$' character
j findLoop1
endFindLoop1:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character


add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderAdd1:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderAdd1	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderAdd1

endRegisterFinderAdd1:
add $s6,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop2:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop2		#Looks for the '$' character
j findLoop2
endFindLoop2:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderAdd2:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderAdd2	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderAdd2

endRegisterFinderAdd2:
add $s4,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop3:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop3		#Looks for the '$' character
j findLoop3
endFindLoop3:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderAdd3:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderAdd3	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderAdd3

endRegisterFinderAdd3:
add $s5,$k0,$zero			#moves the register number to a known location


###################Print out Hex###################
move $k1,$s4
sll $k1, $k1,0x5
addu $k1,$k1,$s5
sll $k1, $k1,0x5
addu $k1,$k1,$s6
sll $k1, $k1,0xb
addu $k1,$k1,$s7

move $a0,$k1
li $v0,34
syscall



b	end

oSubtract:
li  $a3,4
lw  $s7,opcodes($a3)		#loads subtracts opcode into a known place in memory

la	$t0,msg					#load the address of the stored string

####################Encode Register 1###########################
lw	$t2,z					#loads the '$' character

findLoop4:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop4	#Looks for the '$' character
j findLoop4
endFindLoop4:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character


add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderSub1:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderSub1	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderSub1

endRegisterFinderSub1:
add $s6,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop5:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop5		#Looks for the '$' character
j findLoop5
endFindLoop5:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderSub2:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderSub2	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderSub2

endRegisterFinderSub2:
add $s4,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop6:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop6		#Looks for the '$' character
j findLoop6
endFindLoop6:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderSub3:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderSub3	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderSub3

endRegisterFinderSub3:
add $s5,$k0,$zero			#moves the register number to a known location


###################Print out Hex###################
move $k1,$s4
sll $k1, $k1,0x5
addu $k1,$k1,$s5
sll $k1, $k1,0x5
addu $k1,$k1,$s6
sll $k1, $k1,0xb
addu $k1,$k1,$s7

move $a0,$k1
li $v0,34
syscall

b	end




oAnd:
li  $a3,8
lw  $s7,opcodes($a3)		#loads ands opcode into a known place in memory
la	$t0,msg					#load the address of the stored string

####################Encode Register 1###########################
lw	$t2,z					#loads the '$' character

findLoop7:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop7	#Looks for the '$' character
j findLoop7
endFindLoop7:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character


add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderAnd1:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderAnd1	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderAnd1

endRegisterFinderAnd1:
add $s6,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop8:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop8		#Looks for the '$' character
j findLoop8
endFindLoop8:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderAnd2:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderAnd2	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderAnd2

endRegisterFinderAnd2:
add $s4,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop9:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop9		#Looks for the '$' character
j findLoop9
endFindLoop9:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderAnd3:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderAnd3	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderAnd3

endRegisterFinderAnd3:
add $s5,$k0,$zero			#moves the register number to a known location


###################Print out Hex###################
move $k1,$s4
sll $k1, $k1,0x5
addu $k1,$k1,$s5
sll $k1, $k1,0x5
addu $k1,$k1,$s6
sll $k1, $k1,0xb
addu $k1,$k1,$s7

move $a0,$k1
li $v0,34
syscall


b	end


oOr:
li  $a3,12
lw  $s7,opcodes($a3)		#loads ors opcode into a known place in memory
la	$t0,msg					#load the address of the stored string

####################Encode Register 1###########################
lw	$t2,z					#loads the '$' character

findLoop10:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop10	#Looks for the '$' character
j findLoop10
endFindLoop10:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character


add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderOr1:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderOr1	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderOr1

endRegisterFinderOr1:
add $s6,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop11:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop11		#Looks for the '$' character
j findLoop11
endFindLoop11:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderOr2:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderOr2	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderOr2

endRegisterFinderOr2:
add $s4,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop12:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop12		#Looks for the '$' character
j findLoop12
endFindLoop12:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderOr3:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderOr3	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderOr3

endRegisterFinderOr3:
add $s5,$k0,$zero			#moves the register number to a known location


###################Print out Hex###################
move $k1,$s4
sll $k1, $k1,0x5
addu $k1,$k1,$s5
sll $k1, $k1,0x5
addu $k1,$k1,$s6
sll $k1, $k1,0xb
addu $k1,$k1,$s7

move $a0,$k1
li $v0,34
syscall





b	end

oSetLess:
li  $a3,16
lw  $s7,opcodes($a3)		#loads ands opcode into a known place in memory
la	$t0,msg					#load the address of the stored string

####################Encode Register 1###########################
lw	$t2,z					#loads the '$' character

findLoop13:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop13	#Looks for the '$' character
j findLoop13
endFindLoop13:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character


add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderSlt1:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderSlt1	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderSlt1

endRegisterFinderSlt1:
add $s6,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop14:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop14		#Looks for the '$' character
j findLoop14
endFindLoop14:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderSlt2:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderSlt2	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderSlt2

endRegisterFinderSlt2:
add $s4,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop15:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop15		#Looks for the '$' character
j findLoop15
endFindLoop15:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderSlt3:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderSlt3	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderSlt3

endRegisterFinderSlt3:
add $s5,$k0,$zero			#moves the register number to a known location


###################Print out Hex###################
move $k1,$s4
sll $k1, $k1,0x5
addu $k1,$k1,$s5
sll $k1, $k1,0x5
addu $k1,$k1,$s6
sll $k1, $k1,0xb
addu $k1,$k1,$s7

move $a0,$k1
li $v0,34
syscall





























b	end

oBranchEqual:
li $a3,20
lw  $s4,opcodes($a3)		#loads ands opcode into a known place in memory
la	$t0,msg					#load the address of the stored string

####################Encode Register 1###########################
lw	$t2,z					#loads the '$' character

findLoop16:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop16	#Looks for the '$' character
j findLoop16
endFindLoop16:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character


add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderBeq1:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderBeq1	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderBeq1

endRegisterFinderBeq1:
add $s5,$k0,$zero			#moves the register number to a known location


####################Encode Register 2###########################
lw	$t2,z					#loads the '$' character

findLoop17:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop17		#Looks for the '$' character
j findLoop17
endFindLoop17:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderBeq2:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderBeq2	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderBeq2

endRegisterFinderBeq2:
add $s6,$k0,$zero			#moves the register number to a known location


####################Encode Immediate Value###########################
li $t5,1				#loadst he number 1
lw $t2,negative				#loads the negatie sign character
addi	$t0,$t0,2		#advances forward to check for negative

lb $t1,0($t0)			#loads first character of the immediate
beq	$t1, $t2,negativeFound		#checks for a negative sign
j noNegative

negativeFound:			#a negative was found
li $t5,-1				#load -1 into known space
addi	$t0,$t0,1		#advances in string

noNegative:

lw  $a2,n					#newline character into $a2
lw	$t8,ten					#loads in ten
lw	$s0,y					#loads in y
li $k0,0					#initialize counter to zero

lb		$t1,0($t0)			#loads the first character of the immediate

#add $k1,$t1,$zero			#move to known space

readDigits1:
jal 	dloop1
addi	$t0,$t0,1				#advance forward
lb		$t1,0($t0)				#read in the next digit
beq		$a2,$t1,endReadDigits1	#break if reach end of number

j readDigits1				#loop back

dloop1:
mul	$t9,$t9,$t8
lw	$t6,offset				#loads the offset
findDLoop1:
lw  $t2,numberz($t6)			#loads the first content of the table
beq	$t1,$t2,endfindDLoop1	#if the first character is an 'a' goto the a area to further identify
addi	$t6,$t6,4
j findDLoop1
endfindDLoop1:
div	$t6,$t6,$s0
addu	$t9,$t6,$t9
jr $ra


endReadDigits1:

mul $t9,$t9,$t5				#make the number negative if it is

sll $s4,$s4,0x1a			#shift the opcode
sll $s5,$s5,0x15			#shift the first register
sll $s6,$s6,0x10			#shift the second register
sll	$t9,$t9,0x10			#shift immediate val left, then right to remove uneeded F's
srl	$t9,$t9,0x10			
	
addu $s4,$s4,$s6
addu $s4,$s4,$s5
addu $s4,$s4,$t9

move $a0,$s4				#move value to print
li $v0,34					
syscall						#print


b	end


oJump:
li $a3,24					#load opcode location
lw  $s4,opcodes($a3)		#loads ands opcode into a known place in memory
lw	$a1,s					#load space character into $a1
lw  $a2,n					#newline character into $a2
lw	$t8,ten					#loads in ten
lw	$s0,y					#loads in y
li $k0,0					#initialize counter to zero

lb		$t1,0($t0)			#loads the first character in the string into $t1 - should be j
addi	$t0,$t0,1			#advance forward
lb		$t1,0($t0)          #load next digit - should be a space

goPastSpaces:				#Scan through space char's
bne $a1,$t1,endGoPastSpaces	#checks for first digit
addi	$t0,$t0,1			#advance forward
lb		$t1,0($t0)			#load next digit
j goPastSpaces


endGoPastSpaces:			#start reading in digits

add $k1,$t1,$zero			#move to known space

readDigits:
#addi $k0,$k0,1					#counter++
jal 	dloop
addi	$t0,$t0,1				#advance forward
lb		$t1,0($t0)				#read in the next digit
beq		$a2,$t1,endReadDigits	#break if reach end of number

#sll $k1,$k1,0x8					#shifts digits over, and adds next digit
#addu $k1,$k1,$t1
j readDigits				#loop back

dloop:
mul	$t9,$t9,$t8
lw	$t6,offset				#loads the offset
findDLoop:
lw  $t2,numberz($t6)			#loads the first content of the table
beq	$t1,$t2,endfindDLoop	#if the first character is an 'a' goto the a area to further identify
addi	$t6,$t6,4
j findDLoop
endfindDLoop:
div	$t6,$t6,$s0
addu	$t9,$t6,$t9
jr $ra
endReadDigits:				#done reading digits, time to print
div	$t9,$t9,$s0
sll $s4,$s4,0x1a
addu $s4,$s4,$t9


move $a0,$s4				#move value to print
li $v0,34					
syscall						#print


b	end


oLoadWord:
li $a3,28
lw  $s4,opcodes($a3)		#loads ands opcode into a known place in memory


la	$t0,msg					#load the address of the stored string

####################Encode Register 1###########################
lw	$t2,z					#loads the '$' character

findLoop18:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop18	#Looks for the '$' character
j findLoop18
endFindLoop18:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character


add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderLw1:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderLw1	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderLw1

endRegisterFinderLw1:
add $s6,$k0,$zero			#moves the register number to a known location

####################Encode Immediate Value###########################

li $t5,1				#loadst he number 1
lw $t2,negative				#loads the negatie sign character
addi	$t0,$t0,2		#advances forward to check for negative

lb $t1,0($t0)			#loads first character of the immediate
beq	$t1, $t2,negativeFound1		#checks for a negative sign
j noNegative1

negativeFound1:			#a negative was found
li $t5,-1				#load -1 into known space
addi	$t0,$t0,1		#advances in string

noNegative1:

lw  $a2,p					#newline character into $a2
lw	$t8,ten					#loads in ten
lw	$s0,y					#loads in y
li $k0,0					#initialize counter to zero

lb		$t1,0($t0)			#loads the first character of the immediate

#add $k1,$t1,$zero			#move to known space

readDigits2:
jal 	dloop2
addi	$t0,$t0,1				#advance forward
lb		$t1,0($t0)				#read in the next digit
beq		$a2,$t1,endReadDigits2	#break if reach end of number

j readDigits2				#loop back

dloop2:
mul	$t9,$t9,$t8
lw	$t6,offset				#loads the offset
findDLoop2:
lw  $t2,numberz($t6)			#loads the first content of the table
beq	$t1,$t2,endfindDLoop2	#if the first character is an 'a' goto the a area to further identify
addi	$t6,$t6,4
j findDLoop2
endfindDLoop2:
div	$t6,$t6,$s0
addu	$t9,$t6,$t9
jr $ra


endReadDigits2:

mul $t9,$t9,$t5				#make the number negative if it is

####################Encode Register 2###########################
addi	$t0,$t0,2

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderLw2:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderLw2	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderLw2

endRegisterFinderLw2:
add $s5,$k0,$zero			#moves the register number to a known location


sll $s4,$s4,0x1a			#shift the opcode
sll $s5,$s5,0x15			#shift the first register
sll $s6,$s6,0x10			#shift the second register
sll	$t9,$t9,0x10			#shift immediate val left, then right to remove uneeded F's
srl	$t9,$t9,0x10			
	
addu $s4,$s4,$s6
addu $s4,$s4,$s5
addu $s4,$s4,$t9

move $a0,$s4				#move value to print
li $v0,34					
syscall						#print



b	end

oStoreWord:
li $a3,32
lw  $s4,opcodes($a3)		#loads ands opcode into a known place in memory


la	$t0,msg					#load the address of the stored string

####################Encode Register 1###########################
lw	$t2,z					#loads the '$' character

findLoop19:
lb	$t1,0($t0)				#loads the first character in the string into $t1
addi	$t0,$t0,1
beq	$t1,$t2,endFindLoop19	#Looks for the '$' character
j findLoop19
endFindLoop19:

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character


add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderSw1:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderSw1	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderSw1

endRegisterFinderSw1:
add $s6,$k0,$zero			#moves the register number to a known location

####################Encode Immediate Value###########################

li $t5,1				#loadst he number 1
lw $t2,negative				#loads the negatie sign character
addi	$t0,$t0,2		#advances forward to check for negative

lb $t1,0($t0)			#loads first character of the immediate
beq	$t1, $t2,negativeFound2		#checks for a negative sign
j noNegative2

negativeFound2:			#a negative was found
li $t5,-1				#load -1 into known space
addi	$t0,$t0,1		#advances in string

noNegative2:

lw  $a2,p					#newline character into $a2
lw	$t8,ten					#loads in ten
lw	$s0,y					#loads in y
li $k0,0					#initialize counter to zero

lb		$t1,0($t0)			#loads the first character of the immediate

#add $k1,$t1,$zero			#move to known space

readDigits3:
jal 	dloop3
addi	$t0,$t0,1				#advance forward
lb		$t1,0($t0)				#read in the next digit
beq		$a2,$t1,endReadDigits3	#break if reach end of number

j readDigits3				#loop back

dloop3:
mul	$t9,$t9,$t8
lw	$t6,offset				#loads the offset
findDLoop3:
lw  $t2,numberz($t6)			#loads the first content of the table
beq	$t1,$t2,endfindDLoop3	#if the first character is an 'a' goto the a area to further identify
addi	$t6,$t6,4
j findDLoop3
endfindDLoop3:
div	$t6,$t6,$s0
addu	$t9,$t6,$t9
jr $ra


endReadDigits3:

mul $t9,$t9,$t5				#make the number negative if it is

####################Encode Register 2###########################
addi	$t0,$t0,2

#Goes about concatenating 2 characters
lb		$t1,0($t0)			#loads the first character in the string into $t1
addi	$t0,$t0,1
lb		$t3,0($t0)			#loads the second character in the string into $t3

sll $t3, $t3, 0x8			#shifts the first character over
addu $t4, $t3, $t1			#concatinates the first and second character

add $k0,$zero,$zero			#resets k0 counter
lw	$a3,offset				#loads the offset

registerFinderSw2:					#finds which register $t4 describes

lw  $t2,registers($a3)				#loads the first register of the table
beq	$t4,$t2,endRegisterFinderSw2	#checks if the current register is the correct one
addi $k0,$k0,1						#increases the counter
addi $a3,$a3,4						#increase offset by 2
j registerFinderSw2

endRegisterFinderSw2:
add $s5,$k0,$zero			#moves the register number to a known location


sll $s4,$s4,0x1a			#shift the opcode
sll $s5,$s5,0x15			#shift the first register
sll $s6,$s6,0x10			#shift the second register
sll	$t9,$t9,0x10			#shift immediate val left, then right to remove uneeded F's
srl	$t9,$t9,0x10			
	
addu $s4,$s4,$s6
addu $s4,$s4,$s5
addu $s4,$s4,$t9

move $a0,$s4				#move value to print
li $v0,34					
syscall						#print



b	end



end:


.data
x:		.word 30			#prepare to store a large string
y:		.word 4				#the number 4
offset:	.word 0				#the offset in a table
z:		.asciiz	"$"			#the marker of a register
i:		.word 0				#the index
negative:	.asciiz "-"			#the negative character
		.align 2
ten:	.word 10			#the number 10
s:		.asciiz " "			#space character
		.align 2
n:		.asciiz "\n"		#newline character
		.align 2
p:		.asciiz "("			#first parenthesis character
		.align 2
#array storing possible first character values
array1:   
        .asciiz   "a"			#the letter a
		.align 2
        .asciiz   "b"			#the letter b
		.align 2
        .asciiz   "j"			#the letter j
		.align 2
        .asciiz   "l"			#the letter l
		.align 2
        .asciiz   "o"			#the letter o
		.align 2
        .asciiz   "s"			#the letter s

#array storing possible second character values given the first is an a
array2:
		.align 2
        .asciiz   "d"			#the letter d
		.align 2
        .asciiz   "n"			#the letter n

#array storing possible second character values given the first is an s
array3:
		.align 2
        .asciiz   "u"			#the letter u
		.align 2
        .asciiz   "l"			#the letter l
		.align 2
        .asciiz   "w"			#the letter w

#array of opcodes stored for future use
opcodes:
		.align 2
        .word	  	32				#the opcode for add
		.align 2
        .word   	34				#the opcode for subtract
		.align 2
        .word   	36				#the opcode for and
		.align 2
        .word   	37				#the opcode for or
		.align 2
        .word   	42				#the opcode for set less than
		.align 2		
        .word   	4				#the opcode for branch on equal
		.align 2
        .word   	2				#the opcode for jump
		.align 2
        .word		35				#the opcode for load word
		.align 2
        .word		43				#the opcode for store word

#array of possible registers
registers:
		.align 2
        .asciiz   "ze"
		.align 2
        .asciiz   "at"
		.align 2
        .asciiz   "v0"
		.align 2
        .asciiz   "v1"
		.align 2
        .asciiz   "a0"
		.align 2
        .asciiz   "a1"
		.align 2
        .asciiz   "a2"
		.align 2
        .asciiz   "a3"
		.align 2
        .asciiz   "t0"
		.align 2
        .asciiz   "t1"
		.align 2
        .asciiz   "t2"
		.align 2
        .asciiz   "t3"
		.align 2
        .asciiz   "t4"
		.align 2
        .asciiz   "t5"
		.align 2
        .asciiz   "t6"
		.align 2
        .asciiz   "t7"
		.align 2
        .asciiz   "s0"
		.align 2
        .asciiz   "s1"
		.align 2
        .asciiz   "s2"
		.align 2
        .asciiz   "s3"
		.align 2
        .ascii    "s4"
		.align 2
        .asciiz   "s5"
		.align 2
        .asciiz   "s6"
		.align 2
        .asciiz   "s7"
		.align 2
        .asciiz   "t8"
		.align 2
        .asciiz   "t9"
		.align 2
        .asciiz   "k0"
		.align 2
        .asciiz   "k1"
		.align 2
        .asciiz   "gp"
		.align 2
        .asciiz   "sp"
		.align 2
        .asciiz   "fp"
		.align 2
        .asciiz   "ra"

#array of digits 0-9 in ascii
numberz:
		.align 2
        .asciiz   "0"
		.align 2
        .asciiz   "1"
		.align 2
        .asciiz   "2"
		.align 2
        .asciiz   "3"
		.align 2
        .asciiz   "4"
		.align 2
        .asciiz   "5"
		.align 2
        .asciiz   "6"
		.align 2
        .asciiz   "7"
		.align 2
        .asciiz   "8"
		.align 2
        .asciiz   "9"



        .space 300
msg:    .asciiz	""		#place to store input string
		.space 50
result:	.asciiz	""		#output

